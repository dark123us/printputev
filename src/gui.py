#!/usr/bin/python
# -*- coding: utf-8  -*-
import sys, os
from PyQt4 import QtGui, QtCore, QtWebKit, Qt
from PyQt4.QtCore import SIGNAL
from datetime import datetime


class LoadRaznar():
    def __init__(self):
        self._path_dbf = '' #папка с локальными dbf файлами
        self._path_exch = ''#папка обмена - откуда забираются данные и куда будем сбрасывать различные справочники
    
    def load_raznar(self):
        'Загружаем разнарядку с обменника'
        pass
    
    def add_raznar(self):
        'Добавляем данные в dbf файл или изменяем данные'
        pass
    
    def get_raznar(self):
        'Получить данные с локального файла разнарядки'
        pass

class MainForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setWindowTitle(u'Выписка путевых листов')
        self.setMinimumSize(900,530)
        self.central()
        self.init_value()
        self.init_widget()
    
    def central(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)
    
    def init_value(self):
        self.cur_date = QtGui.QDateEdit()
        self.cur_date.setCalendarPopup(True)
        self.cur_date.setDate(QtCore.QDate(datetime.now()))
        self.cur_date.setDisplayFormat('dd.MM.yyyy')
     
    def init_widget(self):
        w = QtGui.QWidget()
        gr = QtGui.QGridLayout()
        gr.addWidget(self.cur_date)
        w.setLayout(gr)
        self.setCentralWidget(w)
