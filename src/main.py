#!/usr/bin/python
# -*- coding: utf-8  -*-
import sys, os
from PyQt4 import QtGui, QtCore, QtWebKit, Qt
from PyQt4.QtCore import SIGNAL
from gui import MainForm  

class main(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setWindowTitle(u'Печать')
        self.setMinimumSize(900,530)
        self.central()
        self.init_value()
        self.init_widget()
    
    def central(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)
    
    def init_value(self):
        #self.inittextdokument()
        
        self.td = QtGui.QTextDocument()
        self.te = QtGui.QTextEdit()
        
        self.pr = QtGui.QPrinter(0)
        self.pr.setOrientation(QtGui.QPrinter.Landscape)
        self.pr.setPageSize(QtGui.QPrinter.A4)
        #self.pr.setPaperSize(QtGui.QPrinter.A4)
        self.pr.setPageMargins(10,10,10,10,0)
        
        s = ''
        self.te.setText(s)
        self.pb = QtGui.QPushButton()
        self.pb.setText(u'Предпросмотр\n и печать')
        self.connect(self.pb, SIGNAL('clicked()'),self.printPreview)
        
        self.pb2 = QtGui.QPushButton()
        self.pb2.setText(u'печать')
        self.connect(self.pb2, SIGNAL('clicked()'),self.printView)
        
        
        
        self.htmlView = QtWebKit.QWebView()
        remHtml = QtCore.QFile("forms/index2.html")
        if (remHtml.open(QtCore.QIODevice.ReadOnly)):
            inH = QtCore.QTextStream(remHtml)
            path = os.getcwd()
            #self.htmlView.settings().setUserStyleSheetUrl(QtCore.QUrl.fromLocalFile(path + "/forms/main.css"))
            self.htmlView.setHtml(inH.readAll())

    def printPreview(self):
        print "printPreview"
        
        pr = QtGui.QPrinter(0)
        pr.setOrientation(QtGui.QPrinter.Landscape)
        #pr.setPageSize(QtGui.QPrinter.A4)
        #pr.setPaperSize(QtGui.QPrinter.A4)
        pr.setPaperSize(QtCore.QSizeF(210,297),QtGui.QPrinter.Millimeter);
        pr.setPageMargins(10,10,10,10,0)

        prpr = QtGui.QPrintPreviewDialog(pr)
       
        prpr.connect(prpr, SIGNAL('paintRequested(QPrinter*)'), self.printPreviewNeeds)
        print "pageSize = ",pr.pageSize(),"paperSize = ", pr.paperSize()
        
        #self.prpr.exec_()
        prpr.exec_()
        pr.setPaperSize(QtCore.QSizeF(210,297),QtGui.QPrinter.Millimeter);
        print "pageSize = ",pr.pageSize(),"paperSize = ", pr.paperSize()
        print "-"*100
    
    def printPreviewNeeds(self, printer):
        print "printPreviewNeeds"
        print QtGui.QPrinter.Landscape, printer.orientation()
        printer.setOrientation(QtGui.QPrinter.Landscape)    
        printer.setPaperSize(QtCore.QSizeF(210,297),QtGui.QPrinter.Millimeter);
        printer.setPageMargins(10,10,10,10,0)
        self.htmlView.print_(printer)
        print "pageSize = ",printer.pageSize(),"paperSize = ", printer.paperSize()
        print "+"*100
     
    def init_widget(self):
        w = QtGui.QWidget()
        gr = QtGui.QGridLayout()
        gr.addWidget(self.pb,0,0)
        gr.addWidget(self.pb2,0,1)
        gr.setSpacing(1)
        gr.addWidget(self.htmlView,1,0,1,2)
        w.setLayout(gr)
        self.setCentralWidget(w)
    
    def printView(self):
        pr = QtGui.QPrinter(0)
        
        pr.setOrientation(QtGui.QPrinter.Landscape)
        print QtGui.QPrinter.Landscape, QtGui.QPrinter.Portrait, pr.orientation()
        pr.setPaperSize(QtCore.QSizeF(210,297),QtGui.QPrinter.Millimeter);
        #pr.setPaperSize(QtCore.QSizeF(297,210),QtGui.QPrinter.Millimeter);
        pr.setPageMargins(10,10,10,10,0)
        print "pageSize = ",pr.pageSize(),"paperSize = ", pr.paperSize()
        self.htmlView.print_(pr)
        print "pageSize = ",pr.pageSize(),"paperSize = ", pr.paperSize()

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    qb = main()
    qb = MainForm()
    qb.show()
    sys.exit(app.exec_())